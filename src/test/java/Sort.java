import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;

public class Sort {

    public static void main(String[] args) throws IOException {

        FileReader fin = new FileReader("C:\\Users\\user\\Desktop\\test.txt");

        int c;
        int number=0;
        boolean ncheck = false;
        ArrayList<Integer> n = new ArrayList<Integer>();
        while ((c = fin.read()) != -1){

            if (c>=48 && c<58){
                number = number*10+Character.getNumericValue((char)c);
                ncheck=true;

            }
            else if(ncheck==true){
                n.add(number);
                number=0;
                ncheck=false;
            }

        }
        if(ncheck==true){
            n.add(number);
            number=0;
            ncheck=false;
        }

        Collections.sort(n);

        PrintWriter fw = new PrintWriter(System.out);
        for (int i : n) {
            fw.println( Integer.toString( i ) + (char) 1 + (char) 1 );
            fw.flush();
        }
    }
}

