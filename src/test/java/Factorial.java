import java.math.BigInteger;

public class Factorial {

    public static BigInteger factorial(int value){
    if(value < 0){
        throw new IllegalArgumentException("Значение должно быть положительным");
    }
    BigInteger result = BigInteger.ONE;
    for (int i = 2; i <= value; i++) {
        result = result.multiply(BigInteger.valueOf(i));
    }
    return result;
}
    public static void main (String []args){
    System.out.println( factorial( 20) );
    }
}

